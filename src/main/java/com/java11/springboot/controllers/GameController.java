package com.java11.springboot.controllers;

import com.java11.springboot.dto.GameMinDTO;
import com.java11.springboot.services.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = "/games")
public class GameController {

    // controlador rest: objeto responsavel por receber as requisições e enviar as respostas
    @Autowired
    private GameService gameService;

    @GetMapping
    public List<GameMinDTO> findAll() {
        List<GameMinDTO> result = gameService.findAll();
        return result;
    }
}

