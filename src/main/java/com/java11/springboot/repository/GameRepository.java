package com.java11.springboot.repository;

import com.java11.springboot.entities.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<Game, Long> {
    // camada de acesso a dados: objeto responsavel por acessar o banco de dados

}

