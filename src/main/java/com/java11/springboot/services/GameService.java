package com.java11.springboot.services;

import com.java11.springboot.dto.GameMinDTO;
import com.java11.springboot.entities.Game;
import com.java11.springboot.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameService {

    // camada de serviço: objeto responsavel por fazer as regras de negocio
    @Autowired
    private GameRepository gameRepository;

    public List<GameMinDTO> findAll() {
        List<Game> result = gameRepository.findAll();
        return result.stream().map(GameMinDTO::new).toList();
    }
}
